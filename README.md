# Ejercicio Practico TDD #

El presente proyecto tiene un ejemplo básico para utilizar la librería de:

* Cucumber en Java.
* JUnit 
* Maven.

En ella se especifican la forma en la que se pueden escribir Historias de Usuarios con escenarios estaticos o dínamicos, la configuración que debe presentar al momento de ejecutar de las pruebas, entre otras cosas.