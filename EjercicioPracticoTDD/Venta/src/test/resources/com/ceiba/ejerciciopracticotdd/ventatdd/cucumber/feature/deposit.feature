# Calculo de ventas.
Feature: Calcular Ventas
Como Cajero del centro comercial La 15, me gustar�a ser capaz de registrar 
los productos que son vendidos, con el fin de obtener el total que debe pagar el 
cliente por la compra realizada. 

    Scenario: Venta sin productos registrados
        Given Un cajero registra ventas sin productos
        When El cajero intente calcular el valor
        Then La venta no se puede realizar. "No presenta productos asociados"

    Scenario: Venta con productos sin precio unitario
        Given Un cajero registra una venta con la siguiente lista de productos:
          | nombre     | cantidad | PrecioU |
          | Producto1  |        2 |    5000 |
          | Producto2  |        1 |       0 |
        When El cajero intente calcular el valor
        Then La venta no se puede realizar. "Hay productos sin precio unitario"

    Scenario: Venta con productos sin cantidad
        Given Un cajero registra una venta con la siguiente lista de productos:
          | nombre     | cantidad | PrecioU |
          | Producto1  |        2 |    5000 |
          | Producto2  |        0 |    2000 |
        When El cajero intente calcular el valor
        Then La venta no se puede realizar. "Hay productos sin cantidad"
    
   Scenario: Venta con informacion completa de los productos, pero que no tienen
            vendedor asociado.
        Given Un cajero registra una venta con la siguiente lista de productos:
          | nombre     | cantidad | PrecioU |
          | Producto1  |        2 |    5000 |
          | Producto2  |        2 |    2000 |
        And la cedula del vendedor es ""
        When El cajero intente calcular el valor
        Then La venta no se puede realizar. "No presenta vendedor asociado"
   
    Scenario: Venta con informacion completa de los productos y con vendedor
            asociado.
        Given Un cajero registra una venta con la siguiente lista de productos:
          | nombre     | cantidad | PrecioU |
          | Producto1  |        1 |    5000 |
          | Producto2  |        1 |    2000 |
        And la cedula del vendedor es "123"
        When El cajero intente calcular el valor
        Then La venta presenta un descuento de "0.0" y un saldo total de "7000.0"

    Scenario: Venta con informacion completa de los productos y con vendedor
            asociado.
        Given Un cajero registra una venta con la siguiente lista de productos:
          | nombre     | cantidad | PrecioU |
          | Producto1  |        1 |    5000 |
          | Producto2  |        1 |    2000 |
          | Producto3  |        1 |    2000 |
        And la cedula del vendedor es "123"
        When El cajero intente calcular el valor
        Then La venta presenta un descuento de "0.0" y un saldo total de "9000.0"

    Scenario: Venta con informacion completa de los productos y con vendedor
            asociado para aplicar el descuento del 20%.
        Given Un cajero registra una venta con la siguiente lista de productos:
          | nombre     | cantidad | PrecioU |
          | Producto1  |        2 |    5000 |
          | Producto2  |        1 |    2000 |
          | Producto3  |        1 |    2000 |
        And la cedula del vendedor es "123"
        When El cajero intente calcular el valor
        Then La venta presenta un descuento de "2800.0" y un saldo total de "11200.0"
   
    Scenario: Venta con informacion completa de los productos y con vendedor
            asociado para aplicar el descuento del 5%.
        Given Un cajero registra una venta con la siguiente lista de productos:
          | nombre     | cantidad | PrecioU |
          | Producto1  |        2 | 2000000 |
        And la cedula del vendedor es "123"
        When El cajero intente calcular el valor
        Then La venta presenta un descuento de "200000.0" y un saldo total de "3800000.0"


    