/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceiba.ejerciciopracticotdd.ventatdd;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Clase que se encarga de correr las pruebas y tener en cuenta los archivos
 * ".feature" que contiene la descripci&oacute;n de los escenarios. La
 * anotaci&oacuten;
 * <code>@Cucumber.Options</code> expresa el formato en el que deseo imprimir
 * las pruebas.
 * @author Edward Rayo Cort�s
 */
@RunWith(Cucumber.class)
@Cucumber.Options(format = {"html:target/cucumber-html-report", "json-pretty:target/cucumber-json-report.json"})
public class RunTest {

    public RunTest() {
    }
}