package com.ceiba.ejerciciopracticotdd.ventatdd;

import com.ceiba.ejerciciopracticotdd.ventatdd.bean.Producto;
import com.ceiba.ejerciciopracticotdd.ventatdd.bean.Venta;
import com.ceiba.ejerciciopracticotdd.ventatdd.logica.CalculaVenta;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;
import org.junit.Assert;

/**
 * Prueba para el calculo de las ventas.
 *
 * @author Edward Rayo Cortés
 */
public class VentaTest {

    /**
     * Bean para administrar la información de la venta.
     */
    private Venta venta;
    /**
     * Clase con la l&oacute;gica necesaria para realizar el calculo de las
     * ventas.
     */
    private CalculaVenta ventaBO;

    /* *************************************************************************
     * GIVEN
     * *************************************************************************
     */
    @Given("^Un cajero registra ventas sin productos$")
    public void Un_cajero_registra_ventas_sin_productos() throws Throwable {
        this.venta = new Venta();
        Assert.assertTrue("Lista esperada vacia", venta.getListaProductos().isEmpty());
    }

    @Given("^Un cajero registra una venta con la siguiente lista de productos:$")
    public void Un_cajero_registra_una_venta_con_la_siguiente_lista_de_productos(List<ProductoItem> listaProductos) throws Throwable {
        this.venta = new Venta();
        Producto producto;
        for (ProductoItem prodItem : listaProductos) {
            producto = new Producto();
            producto.setNombre(prodItem.nombre);
            producto.setCantidad(prodItem.cantidad);
            producto.setValorUnitario(prodItem.precioU);
            venta.getListaProductos().add(producto);
        }
        Assert.assertFalse("Lista esperada Llena", venta.getListaProductos().isEmpty());
    }

    /**
     * Carga la cedula del vendedor a la venta.
     *
     * @param cedulaVendedor C&eacute;dula del vendedor.
     * @throws Throwable
     */
    @Given("^la cedula del vendedor es \"([^\"]*)\"$")
    public void la_cedula_del_vendedor_es(String cedulaVendedor) throws Throwable {
        if (this.venta == null) {
            this.venta = new Venta();
        }
        this.venta.setCdVendedor(cedulaVendedor);
    }

    /* *************************************************************************
     * WHEN
     * *************************************************************************
     */
    @When("^El cajero intente calcular el valor$")
    public void El_cajero_intente_calcular_el_valor() throws Throwable {
        ventaBO = new CalculaVenta();
        ventaBO.calcularVenta(this.venta);
    }

    /* *************************************************************************
     * THEN
     * *************************************************************************
     */
    @Then("^La venta no se puede realizar. \"([^\"]*)\"$")
    public void La_venta_no_se_puede_realizar(String mensajeEsperado)
            throws Throwable {
        Assert.assertEquals("Esperado: " + mensajeEsperado
                + "Real: " + ventaBO.getExcepciones(), mensajeEsperado, ventaBO.getExcepciones());
    }

    @Then("^La venta presenta un descuento de \"([^\"]*)\" y un saldo total de \"([^\"]*)\"$")
    public void La_venta_presenta_un_descuento_de_y_un_saldo_total_de(String descuento,
            String saldoTotal) throws Throwable {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("Descuento esperado: ");
        mensaje.append(descuento);
        mensaje.append(" - Descuento real: ");
        mensaje.append(ventaBO.getValorDescuento());
        mensaje.append(" / Total a pagar esperado: ");
        mensaje.append(saldoTotal);
        mensaje.append(" - Total a pagar real: ");
        mensaje.append(ventaBO.getTotalAPagar());
        Assert.assertTrue(mensaje.toString(), Float.parseFloat(descuento) == ventaBO.getValorDescuento()
                && Float.parseFloat(saldoTotal) == ventaBO.getTotalAPagar());
    }

    /**
     * Clase para administrar la información que se envia a traves de las tablas
     * de los ficheros "*.feature"; Se realiza de esta manera por buenas
     * practias, ya que de esta manera, se reduce acoplamiento entre los
     * escenarios y dominios; y se da un mayor control a la información.
     */
    public static class ProductoItem {

        private String nombre;
        private int cantidad;
        private float precioU;
    }
}
