/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceiba.ejerciciopracticotdd.ventatdd.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Edward Rayo Cort�s
 */
public class Venta {

    /**
     * Almacena el c�digo del vendedor que realiz&oacute; la venta.
     */
    private String cdVendedor;
    /**
     * Productos que se compran en la venta.
     */
    private List<Producto> listaProductos;
    
    public Venta() {
        listaProductos = new ArrayList<Producto>();
    }

    public String getCdVendedor() {
        return cdVendedor;
    }

    public void setCdVendedor(String cdVendedor) {
        this.cdVendedor = cdVendedor;
    }

    public List<Producto> getListaProductos() {
        return listaProductos;
    }
    
}
