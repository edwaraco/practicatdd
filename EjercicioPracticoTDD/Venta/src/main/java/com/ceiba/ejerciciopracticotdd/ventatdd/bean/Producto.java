package com.ceiba.ejerciciopracticotdd.ventatdd.bean;

/**
 * Clase que administra la información del producto que haya 
 * comprado un cliente.
 * @author Edward Rayo Cortés
 */
public class Producto {

    /**
     * Administra el nombre del producto.
     */
    private String nombre;
    /**
     * Cantidad que se compra del producto.
     */
    private int cantidad;
    /**
     * Precio unitario.
     */
    private float valorUnitario;
    
    public Producto() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(float valorUnitario) {
        this.valorUnitario = valorUnitario;
    }
    
}
