/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ceiba.ejerciciopracticotdd.ventatdd.logica;

import com.ceiba.ejerciciopracticotdd.ventatdd.bean.Producto;
import com.ceiba.ejerciciopracticotdd.ventatdd.bean.Venta;
import java.util.List;

/**
 *
 * @author Edward Rayo Cortés
 */
public class CalculaVenta {

    /**
     * Guarda las excepciones de las validaciones que no se cumplen.
     */
    private String excepciones;
    /**
     * Guarda el total a pagar.
     */
    private float totalAPagar;
    /**
     * Guarda el total a pagar.
     */
    private float valorDescuento;

    public CalculaVenta() {
    }

    public String getExcepciones() {
        return excepciones;
    }

    public void setExcepciones(String excepciones) {
        this.excepciones = excepciones;
    }

    public float getTotalAPagar() {
        return totalAPagar;
    }

    public float getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(float valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    /**
     * Valora si la información de la lista de los productos se encuentra
     * completa.
     *
     * @return boolean <code>true</code>: Informaci&oacute; de los productos de
     * forma completa. <code>false</code>: Informaci&oacute; de los productos de
     * forma incompleta.
     */
    private boolean isInformacionCompletaProducto(List<Producto> listaProducto) {
        boolean swInfoCompleta = true;
        if (listaProducto.isEmpty()) {
            excepciones = "No presenta productos asociados";
            swInfoCompleta = false;
        } else {
            for (Producto producto : listaProducto) {
                if (producto.getCantidad() == 0) {
                    excepciones = "Hay productos sin cantidad";
                    swInfoCompleta = false;
                }
                if (producto.getValorUnitario() == 0) {
                    excepciones = "Hay productos sin precio unitario";
                    swInfoCompleta = false;
                }
            }
        }
        return swInfoCompleta;
    }

    /**
     * Permite verificar si se asocio un vendedor a la venta.
     *
     * @param cdVendedor
     * @return boolean <code>true</code>: Presenta vendedor. <code>false</code>:
     * No presenta vendedor.
     */
    public boolean swVendedorPresente(String cdVendedor) {
        boolean swVendedor = true;
        if (!(cdVendedor != null && !"".equals(cdVendedor))) {
            excepciones = "No presenta vendedor asociado";
            swVendedor = false;

        }
        return swVendedor;
    }

    public float calculcarDescuento(int cantidadProducto, float totalVenta) {
        float descuento = 0;
        if (cantidadProducto > 3) {
            descuento = (float) (totalVenta * 0.2);
        } else if (totalVenta > 2000000) {
            descuento = (float) (totalVenta * 0.05);
        }
        return descuento;
    }

    /**
     * Calcula el total a pagar por parte del cliente.
     */
    public void calcularVenta(Venta venta) {
        int cantidadProductos = 0;
        if (this.isInformacionCompletaProducto(venta.getListaProductos())
                && this.swVendedorPresente(venta.getCdVendedor())) {
            for (Producto producto : venta.getListaProductos()) {
                this.totalAPagar += producto.getCantidad() * producto.getValorUnitario();
                cantidadProductos += producto.getCantidad();
            }
            this.valorDescuento = this.calculcarDescuento(cantidadProductos,
                    this.totalAPagar);
            this.totalAPagar -= this.valorDescuento;
        }
    }
}
