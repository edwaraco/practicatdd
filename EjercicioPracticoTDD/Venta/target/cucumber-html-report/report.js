$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("com\\ceiba\\ejerciciopracticotdd\\ventatdd\\cucumber\\feature\\deposit.feature");
formatter.feature({
  "id": "calcular-ventas",
  "description": "Como Cajero del centro comercial La 15, me gustar�a ser capaz de registrar \r\nlos productos que son vendidos, con el fin de obtener el total que debe pagar el \r\ncliente por la compra realizada.",
  "name": "Calcular Ventas",
  "keyword": "Feature",
  "line": 2,
  "comments": [
    {
      "value": "# Calculo de ventas.",
      "line": 1
    }
  ]
});
formatter.scenario({
  "id": "calcular-ventas;venta-sin-productos-registrados",
  "description": "",
  "name": "Venta sin productos registrados",
  "keyword": "Scenario",
  "line": 7,
  "type": "scenario"
});
formatter.step({
  "name": "Un cajero registra ventas sin productos",
  "keyword": "Given ",
  "line": 8
});
formatter.match({
  "location": "VentaTest.Un_cajero_registra_ventas_sin_productos()"
});
formatter.result({
  "duration": 216040586,
  "status": "passed"
});
formatter.step({
  "name": "El cajero intente calcular el valor",
  "keyword": "When ",
  "line": 9
});
formatter.match({
  "location": "VentaTest.El_cajero_intente_calcular_el_valor()"
});
formatter.result({
  "duration": 106158,
  "status": "passed"
});
formatter.step({
  "name": "La venta no se puede realizar. \"No presenta productos asociados\"",
  "keyword": "Then ",
  "line": 10
});
formatter.match({
  "arguments": [
    {
      "val": "No presenta productos asociados",
      "offset": 32
    }
  ],
  "location": "VentaTest.La_venta_no_se_puede_realizar(String)"
});
formatter.result({
  "duration": 4233778,
  "status": "passed"
});
formatter.scenario({
  "id": "calcular-ventas;venta-con-productos-sin-precio-unitario",
  "description": "",
  "name": "Venta con productos sin precio unitario",
  "keyword": "Scenario",
  "line": 12,
  "type": "scenario"
});
formatter.step({
  "name": "Un cajero registra una venta con la siguiente lista de productos:",
  "keyword": "Given ",
  "line": 13,
  "rows": [
    {
      "cells": [
        "nombre",
        "cantidad",
        "PrecioU"
      ],
      "line": 14
    },
    {
      "cells": [
        "Producto1",
        "2",
        "5000"
      ],
      "line": 15
    },
    {
      "cells": [
        "Producto2",
        "1",
        "0"
      ],
      "line": 16
    }
  ]
});
formatter.match({
  "location": "VentaTest.Un_cajero_registra_una_venta_con_la_siguiente_lista_de_productos(VentaTest$ProductoItem\u003e)"
});
formatter.result({
  "duration": 30337655,
  "status": "passed"
});
formatter.step({
  "name": "El cajero intente calcular el valor",
  "keyword": "When ",
  "line": 17
});
formatter.match({
  "location": "VentaTest.El_cajero_intente_calcular_el_valor()"
});
formatter.result({
  "duration": 91074,
  "status": "passed"
});
formatter.step({
  "name": "La venta no se puede realizar. \"Hay productos sin precio unitario\"",
  "keyword": "Then ",
  "line": 18
});
formatter.match({
  "arguments": [
    {
      "val": "Hay productos sin precio unitario",
      "offset": 32
    }
  ],
  "location": "VentaTest.La_venta_no_se_puede_realizar(String)"
});
formatter.result({
  "duration": 98057,
  "status": "passed"
});
formatter.scenario({
  "id": "calcular-ventas;venta-con-productos-sin-cantidad",
  "description": "",
  "name": "Venta con productos sin cantidad",
  "keyword": "Scenario",
  "line": 20,
  "type": "scenario"
});
formatter.step({
  "name": "Un cajero registra una venta con la siguiente lista de productos:",
  "keyword": "Given ",
  "line": 21,
  "rows": [
    {
      "cells": [
        "nombre",
        "cantidad",
        "PrecioU"
      ],
      "line": 22
    },
    {
      "cells": [
        "Producto1",
        "2",
        "5000"
      ],
      "line": 23
    },
    {
      "cells": [
        "Producto2",
        "0",
        "2000"
      ],
      "line": 24
    }
  ]
});
formatter.match({
  "location": "VentaTest.Un_cajero_registra_una_venta_con_la_siguiente_lista_de_productos(VentaTest$ProductoItem\u003e)"
});
formatter.result({
  "duration": 1007111,
  "status": "passed"
});
formatter.step({
  "name": "El cajero intente calcular el valor",
  "keyword": "When ",
  "line": 25
});
formatter.match({
  "location": "VentaTest.El_cajero_intente_calcular_el_valor()"
});
formatter.result({
  "duration": 41346,
  "status": "passed"
});
formatter.step({
  "name": "La venta no se puede realizar. \"Hay productos sin cantidad\"",
  "keyword": "Then ",
  "line": 26
});
formatter.match({
  "arguments": [
    {
      "val": "Hay productos sin cantidad",
      "offset": 32
    }
  ],
  "location": "VentaTest.La_venta_no_se_puede_realizar(String)"
});
formatter.result({
  "duration": 88000,
  "status": "passed"
});
formatter.scenario({
  "id": "calcular-ventas;venta-con-productos-sin-cantidad",
  "description": "",
  "name": "Venta con productos sin cantidad",
  "keyword": "Scenario",
  "line": 28,
  "type": "scenario"
});
formatter.step({
  "name": "Un cajero registra una venta con la siguiente lista de productos:",
  "keyword": "Given ",
  "line": 29,
  "rows": [
    {
      "cells": [
        "nombre",
        "cantidad",
        "PrecioU"
      ],
      "line": 30
    },
    {
      "cells": [
        "Producto1",
        "2",
        "5000"
      ],
      "line": 31
    },
    {
      "cells": [
        "Producto2",
        "2",
        "2000"
      ],
      "line": 32
    }
  ]
});
formatter.match({
  "location": "VentaTest.Un_cajero_registra_una_venta_con_la_siguiente_lista_de_productos(VentaTest$ProductoItem\u003e)"
});
formatter.result({
  "duration": 923861,
  "status": "passed"
});
formatter.step({
  "name": "la cedula del vendedor es \"\"",
  "keyword": "And ",
  "line": 33
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 27
    }
  ],
  "location": "VentaTest.la_cedula_del_vendedor_es(String)"
});
formatter.result({
  "duration": 98896,
  "status": "passed"
});
formatter.step({
  "name": "El cajero intente calcular el valor",
  "keyword": "When ",
  "line": 34
});
formatter.match({
  "location": "VentaTest.El_cajero_intente_calcular_el_valor()"
});
formatter.result({
  "duration": 58387,
  "status": "passed"
});
formatter.step({
  "name": "La venta no se puede realizar. \"No presenta vendedor asociado\"",
  "keyword": "Then ",
  "line": 35
});
formatter.match({
  "arguments": [
    {
      "val": "No presenta vendedor asociado",
      "offset": 32
    }
  ],
  "location": "VentaTest.La_venta_no_se_puede_realizar(String)"
});
formatter.result({
  "duration": 97778,
  "status": "passed"
});
});